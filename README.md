
## Описание

Простой сервер на Nest JS которые содержит модуль авторизации, модуль пользователей. В качестве основной бд используется postgres, в качестве хранилища для jwt - redis. Обе БД поднимаются в докер-контейнерах. Для редиса используется дефолтный конфиг.


Авторизация выполнена при помощи JWT токенов (access, refresh). 

Документация методов API - [Swagger](http://localhost:3000/swagger)


## Установка

```bash
$ npm install --force
```

## Запуск приложения

```bash
# 1. Запуск контейнеров
$ docker-compose up -d

# 2. Поднятие миграций в базе
$ npm run typeorm:migration:run

# 3. Поднятие сид даныых (для ролей пользователя)
$ npm run typeorm:seed:run

# 4. Запуск приложения в режиме разрабаотки
$ npm run start:dev
```

## Стек
```
- Nest JS 
- TypeORM
- Redis
- Docker-Compose
```


## Контактная информация

- Email - cnyh923@gmail.com
- Telegram - @Yugay_S
- hh.ru - [Югай Сергей](https://hh.ru/resume/9cd72968ff0b9e8c250039ed1f724f54617662)

