import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ConfigService } from '@nestjs/config'
import { ConfigVariables } from '@config'
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt'
import {
    AccessTokenPayloadBO,
    JwtTokenPayloadBo,
    RefreshTokenPayloadBO,
} from '@auth/bos'
import { Request } from 'express'
import { RedisRepository } from '@auth/redis.repository'

const extractJwtFromRequest = (req: Request & { accessToken: string }) => {
    const token = ExtractJwt.fromAuthHeaderAsBearerToken()(req)
    req.accessToken = token
    return token
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly configService: ConfigService<ConfigVariables>,
        private readonly redisRepository: RedisRepository,
    ) {
        super({
            jwtFromRequest: extractJwtFromRequest,
            ignoreExpiration: false,
            secretOrKey: configService.get('jwtSecret'),
            passReqToCallback: true,
        } as StrategyOptions)
    }

    async validate(
        req: Request & { accessToken: string },
        payload: JwtTokenPayloadBo,
    ): Promise<AccessTokenPayloadBO> {
        if ((payload as JwtTokenPayloadBo<RefreshTokenPayloadBO>).refresh) {
            throw new UnauthorizedException()
        }
        const blacklisted = await this.redisRepository.isTokenBlackListed(
            req.accessToken,
        )
        if (blacklisted) throw new UnauthorizedException()

        const { userId, email, roles } =
            payload as JwtTokenPayloadBo<AccessTokenPayloadBO>
        return { userId, email, roles }
    }
}
