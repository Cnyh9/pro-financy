import { Module } from '@nestjs/common'
import { AuthService } from '@auth/auth.service'
import { AuthController } from '@auth/auth.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from '../database/entities'
import { TokenResponseService } from '@auth/tokenResponse.service'
import { JwtModule } from '@nestjs/jwt'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { nestJwtConfig } from '@config'
import { JwtStrategy } from '@auth/jwt'
import { RedisRepository } from '@auth/redis.repository'

@Module({
    imports: [
        TypeOrmModule.forFeature([UserEntity]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: nestJwtConfig,
        }),
        ConfigModule,
    ],
    controllers: [AuthController],
    providers: [
        AuthService,
        TokenResponseService,
        JwtStrategy,
        RedisRepository,
    ],
    exports: [AuthService],
})
export class AuthModule {}
