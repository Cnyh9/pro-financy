import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import { ConfigVariables } from '@config'
import { RoleEntity } from '../database/entities'
import {
    AccessTokenPayloadBO,
    RefreshTokenPayloadBO,
    TokenResponseBo,
} from '@auth/bos'

@Injectable()
export class TokenResponseService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService<ConfigVariables>,
    ) {}

    createTokenResponse(
        userId: number,
        email: string,
        userRoles: RoleEntity[],
    ): TokenResponseBo {
        const accessTokenExpires = this.configService.get(
            'jwtAccessTokenExpires',
            { infer: true },
        )
        const refreshTokenExpires = this.configService.get(
            'jwtRefreshTokenExpires',
            { infer: true },
        )
        const roles = userRoles.map(({ role }) => role)

        return {
            accessToken: this.jwtService.sign(
                { userId, email, roles } as AccessTokenPayloadBO,
                { expiresIn: accessTokenExpires },
            ),
            refreshToken: this.jwtService.sign(
                {
                    refresh: true,
                    userId,
                } as RefreshTokenPayloadBO,
                { expiresIn: refreshTokenExpires },
            ),
            expiresIn: +accessTokenExpires,
        }
    }
}
