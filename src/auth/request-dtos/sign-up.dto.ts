import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsString, MinLength } from 'class-validator'

export class SignUpDto {
    @ApiProperty()
    @IsEmail({}, { message: 'Строка должна содержать email' })
    email: string

    @ApiProperty()
    @IsString({ message: 'Пароль должен быть строкой' })
    @MinLength(8, { message: 'Пароль должен содержать не менее 8 символов' })
    password: string
}
