import { ApiProperty } from '@nestjs/swagger'
import { IsJWT } from 'class-validator'

export class RefreshTokensDtoQuery {
    @ApiProperty()
    @IsJWT({ message: 'Invalid JWT' })
    refreshToken: string
}
