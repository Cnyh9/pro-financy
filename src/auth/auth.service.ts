import {
    BadRequestException,
    Injectable,
    NotFoundException,
    UnauthorizedException,
} from '@nestjs/common'
import { SignInDto, SignUpDto } from '@auth/request-dtos'
import { InjectRepository } from '@nestjs/typeorm'
import { UserEntity } from '../database/entities'
import { Repository } from 'typeorm'
import * as bcrypt from 'bcrypt'
import { TokenResponseService } from '@auth/tokenResponse.service'
import {
    AccessTokenPayloadBO,
    JwtTokenPayloadBo,
    RefreshTokenPayloadBO,
    TokenResponseBo,
} from '@auth/bos'
import { JwtService } from '@nestjs/jwt'
import { RedisRepository } from '@auth/redis.repository'

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        private readonly tokenService: TokenResponseService,
        private jwtService: JwtService,
        private redisRepository: RedisRepository,
    ) {}

    async signUp(signUpDto: SignUpDto): Promise<TokenResponseBo> {
        const user = await this.userRepository.findOneBy({
            email: signUpDto.email,
        })

        if (user) {
            throw new BadRequestException(
                `User with email ${signUpDto.email} already exist`,
            )
        }

        signUpDto.password = await this.getHash(signUpDto.password)
        const newUser = await this.userRepository.create({
            email: signUpDto.email,
            password: signUpDto.password,
            roles: [{ role: 'user' }],
        })
        const { id, email, roles } = await this.userRepository.save(newUser)
        return this.tokenService.createTokenResponse(id, email, roles)
    }

    async signIn(body: SignInDto): Promise<TokenResponseBo> {
        const user = await this.userRepository.findOne({
            where: { email: body.email },
            relations: { roles: true },
            select: {
                id: true,
                email: true,
                password: true,
                roles: { role: true },
            },
        })

        if (!user) {
            throw new NotFoundException(
                `User with email ${body.email} does not exist`,
            )
        }
        const comparedPassword = await this.compareHash(
            body.password,
            user.password,
        )
        if (!comparedPassword)
            throw new UnauthorizedException('Incorrect password')

        return this.tokenService.createTokenResponse(
            user.id,
            user.email,
            user.roles,
        )
    }

    async logout(token: string): Promise<void> {
        await this.redisRepository.addTokenToBlackList(token)
    }

    async refreshToken(refreshToken: string): Promise<TokenResponseBo> {
        try {
            const blacklisted = await this.redisRepository.isTokenBlackListed(
                refreshToken,
            )
            if (blacklisted) throw new UnauthorizedException()

            const { refresh, userId } =
                this.jwtService.verify<
                    JwtTokenPayloadBo<RefreshTokenPayloadBO>
                >(refreshToken)
            if (!refresh) throw new UnauthorizedException()
            await this.redisRepository.addTokenToBlackList(refreshToken)

            const { email, name, roles } = await this.userRepository.findOne({
                where: { id: userId },
                relations: { roles: true },
            })

            return this.tokenService.createTokenResponse(userId, email, roles)
        } catch (e) {
            console.log(e)
            throw new UnauthorizedException()
        }
    }

    private async getHash(password: string): Promise<string> {
        const salt = await bcrypt.genSalt(10)
        return await bcrypt.hash(password, salt)
    }

    private async compareHash(
        password: string,
        hash: string,
    ): Promise<boolean> {
        return await bcrypt.compare(password, hash)
    }

    verifyToken(token: string): AccessTokenPayloadBO {
        return this.jwtService.verify(token)
    }
}
