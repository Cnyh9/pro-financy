import { Injectable } from '@nestjs/common'
import { InjectRedis } from '@liaoliaots/nestjs-redis'
import Redis from 'ioredis'
import { ConfigVariables } from '@config'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class RedisRepository {
    constructor(
        @InjectRedis() private readonly redisClient: Redis,
        private configService: ConfigService<ConfigVariables>,
    ) {}

    async addTokenToBlackList(token: string): Promise<void> {
        const ttl = this.configService.get('jwtAccessTokenExpires')
        await this.redisClient.set(`tokens-blacklist:${token}`, 1, 'EX', ttl)
    }

    async isTokenBlackListed(token: string): Promise<boolean> {
        const result = await this.redisClient.exists(
            `tokens-blacklist:${token}`,
        )
        return result !== 0
    }
}
