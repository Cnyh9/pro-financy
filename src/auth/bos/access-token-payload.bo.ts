export interface AccessTokenPayloadBO {
    userId: number
    email: string
    roles: string[]
}
