export interface RefreshTokenPayloadBO {
    refresh: true
    userId: number
}
