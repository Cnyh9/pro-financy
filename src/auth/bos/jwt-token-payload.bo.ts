export type JwtTokenPayloadBo<T = unknown> = { iat: number; exp: number } & T
