import { Body, Controller, Get, Post, Put, Query, Res } from '@nestjs/common'
import { AuthService } from '@auth/auth.service'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { SignInDto, SignUpDto } from '@auth/request-dtos/'
import { TokenResponseBo } from '@auth/bos'
import { TokenResponseDto } from '@auth/response-dtos'
import { Response } from 'express'
import { AccessToken, Auth } from '@common/decorators'

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('sign-up')
    @ApiOperation({ summary: 'Регистрация нового пользователя' })
    @ApiOkResponse({ type: TokenResponseDto })
    async signUp(
        @Body() body: SignUpDto,
        @Res({ passthrough: true }) res: Response,
    ): Promise<TokenResponseBo> {
        const tokenResponse = await this.authService.signUp(body)
        await res.cookie('accessToken', tokenResponse.accessToken, {
            httpOnly: true,
        })
        await res.cookie('refreshToken', tokenResponse.refreshToken, {
            httpOnly: true,
        })
        return tokenResponse
    }

    @Post('sign-in')
    @ApiOperation({ summary: 'Логин' })
    @ApiOkResponse({ type: TokenResponseDto })
    async login(
        @Body() body: SignInDto,
        @Res({ passthrough: true }) res: Response,
    ): Promise<TokenResponseBo> {
        const tokenResponse = await this.authService.signIn(body)
        await res.cookie('accessToken', tokenResponse.accessToken, {
            httpOnly: true,
        })
        await res.cookie('refreshToken', tokenResponse.refreshToken, {
            httpOnly: true,
        })
        return tokenResponse
    }

    @Post('logout')
    @ApiOperation({ summary: 'Выход из аккаунта' })
    @Auth()
    async logout(@AccessToken() token: string) {
        await this.authService.logout(token)
    }

    @Put('token-refresh')
    @ApiOperation({ summary: 'Обновление токенов' })
    @ApiOkResponse({ type: TokenResponseDto })
    async refreshToken(
        @Query('refreshToken') refreshToken: string,
        @Res({ passthrough: true }) res: Response,
    ): Promise<TokenResponseDto> {
        const tokenResponse = await this.authService.refreshToken(refreshToken)
        await res.cookie('accessToken', tokenResponse.accessToken, {
            httpOnly: true,
        })
        await res.cookie('refreshToken', tokenResponse.refreshToken, {
            httpOnly: true,
        })
        return tokenResponse
    }

    @Get('check')
    @ApiOperation({ summary: 'Проверка валидности токена' })
    @Auth()
    async checkToken(): Promise<void> {
        return
    }
}
