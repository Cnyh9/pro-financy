import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsString } from 'class-validator'

export class UserDto {
    @ApiProperty()
    @IsEmail({}, { message: 'Строка должна содержать email' })
    email: string

    @ApiProperty()
    @IsString({ message: 'Имя должно быть строкой' })
    name: string
}
