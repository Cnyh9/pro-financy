import { forwardRef, Module } from '@nestjs/common'
import { UserService } from './user.service'
import { UserController } from './user.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from '../database/entities'
import { AuthModule } from '@auth/auth.module'
import { FileModule } from '@files/file.module'

@Module({
    controllers: [UserController],
    providers: [UserService],
    imports: [
        TypeOrmModule.forFeature([UserEntity]),
        forwardRef(() => AuthModule),
        AuthModule,
        FileModule,
    ],
    exports: [UserService],
})
export class UserModule {}
