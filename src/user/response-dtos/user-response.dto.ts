import { ApiProperty } from '@nestjs/swagger'

type UserRoles = 'user' | 'admin'
export class UserResponseDto {
    @ApiProperty()
    id: number
    @ApiProperty()
    email: string
    @ApiProperty()
    name: string
    @ApiProperty()
    avatar: string

    @ApiProperty()
    roles: UserRoles[]
}
