import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Put,
    UploadedFile,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common'
import { UserService } from './user.service'
import {
    ApiBody,
    ApiConsumes,
    ApiOkResponse,
    ApiOperation,
    ApiTags,
} from '@nestjs/swagger'
import { AccessToken, Auth, Roles, User } from '@common/decorators'
import { UserResponseDto } from './response-dtos'
import { RolesGuard } from '@common/guards'
import { UserDto } from './request-dtos/user.dto'
import { FileInterceptor } from '@nestjs/platform-express'
import { AvatarDto } from './request-dtos/avatar.dto'
import { AuthService } from '@auth/auth.service'

@Controller('users')
@ApiTags('Users')
@Auth()
@UseGuards(RolesGuard)
@Roles('user')
export class UserController {
    constructor(
        private readonly userService: UserService,
        private readonly authService: AuthService,
    ) {}

    @Get('/me')
    @ApiOperation({ summary: 'Получение данных профиля' })
    @ApiOkResponse({ type: UserResponseDto })
    async getUserInfo(
        @User('userId') userId: number,
    ): Promise<UserResponseDto> {
        return this.userService.getById(userId)
    }

    @Put('user/:id')
    @ApiOperation({ summary: 'Обновить данные пользователя' })
    @ApiOkResponse({ type: UserResponseDto })
    async update(
        @Param('id', ParseIntPipe) userId: number,
        @Body() body: UserDto,
    ): Promise<UserResponseDto> {
        return await this.userService.update(userId, body)
    }

    @Patch('user/:id')
    @ApiOperation({ summary: 'Обновить аватар пользователя' })
    @ApiConsumes('multipart/form-data')
    @ApiBody({ type: AvatarDto })
    @UseInterceptors(FileInterceptor('avatar'))
    async updateAvatar(
        @Param('id') userId: number,
        @UploadedFile() avatar: Express.Multer.File,
    ) {
        return await this.userService.updateAvatar(userId, avatar)
    }

    @Delete('user')
    @ApiOperation({ summary: 'Удалить профиль' })
    async delete(
        @AccessToken() accessToken: string,
        @User('userId') userId: number,
    ): Promise<void> {
        await this.authService.logout(accessToken)
        await this.userService.delete(userId)
    }
}
