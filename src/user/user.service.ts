import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { UserEntity } from '../database/entities'
import { UserResponseDto } from './response-dtos'
import { UserDto } from './request-dtos/user.dto'
import { FileService } from '@files/file.service'

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private userRepository: Repository<UserEntity>,
        private readonly fileService: FileService,
    ) {}

    async getById(id: number): Promise<UserResponseDto> {
        const user = await this.userRepository
            .createQueryBuilder('user')
            .select([
                'user.id as id',
                'user.email as email',
                'user.name as name',
                'user.avatar as avatar',
                'user.createdAt as "createdAt"',
                'user.updatedAt as "updatedAt"',
                'array_agg(roles.role) as roles',
            ])
            .where('user.id = :id', { id })
            .leftJoin('user.roles', 'roles')
            .groupBy('user.id')
            .getRawOne()

        if (!user)
            throw new NotFoundException(`User with id ${id} does not exist`)
        return user
    }

    async update(userId: number, body: UserDto): Promise<UserResponseDto> {
        const user = await this.userRepository.findOne({
            where: { id: userId },
        })
        if (!user)
            throw new NotFoundException(`User with id ${userId} does not exist`)
        user.email = body.email
        user.name = body.name
        await this.userRepository.save(user)
        return this.getById(userId)
    }

    async updateAvatar(userId: number, file: Express.Multer.File) {
        const user = await this.userRepository.findOneBy({ id: userId })
        if (!user)
            throw new NotFoundException(`User with id ${userId} does not exist`)
        const avatar = await this.fileService.filter(file, userId.toString())
        user.avatar = avatar
        await this.userRepository.save(user)
        return await this.getById(userId)
    }

    async delete(userId: number): Promise<void> {
        await this.userRepository.delete(userId)
    }
}
