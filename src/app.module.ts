import { ConfigModule, ConfigService } from '@nestjs/config'
import { Module } from '@nestjs/common'
import { configuration, redisConfigFactory } from '@config'
import { TypeOrmModule } from '@nestjs/typeorm'
import ormconfig from './typeorm.config'
import { RedisModule } from '@liaoliaots/nestjs-redis'
import { AuthModule } from '@auth/auth.module'
import { UserModule } from './user/user.module'
import { FileModule } from '@files/file.module'

@Module({
    imports: [
        ConfigModule.forRoot({ load: [configuration] }),
        TypeOrmModule.forRoot({ ...ormconfig }),
        RedisModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: redisConfigFactory,
        }),
        AuthModule,
        UserModule,
        FileModule,
    ],
})
export class AppModule {}
