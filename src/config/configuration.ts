
export const configuration = () => ({
    port: parseInt(process.env.PORT as string, 10) || 3000,
    redisHost: process.env.REDIS_HOST,
    redisPort: process.env.REDIS_PORT,
    jwtSecret: process.env.JWT_SECRET,
    jwtAccessTokenExpires: parseInt(process.env.JWT_ACCESS_TOKEN_EXPIRES),
    jwtRefreshTokenExpires: parseInt(process.env.JWT_REFRESH_TOKEN_EXPIRES),
})
