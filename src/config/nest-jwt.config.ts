import { ConfigService } from '@nestjs/config'
import { ConfigVariables } from './config.interface'
import { JwtModuleOptions } from '@nestjs/jwt'

export const nestJwtConfig = (
    config: ConfigService<ConfigVariables>,
): JwtModuleOptions => ({
    secret: config.get('jwtSecret'),
    signOptions: {
        expiresIn: config.get('jwtAccessTokenExpires'),
    },
})
