import { ConfigService } from '@nestjs/config'
import { ConfigVariables } from './config.interface'
import { RedisModuleOptions } from '@liaoliaots/nestjs-redis'

export const redisConfigFactory = (
    config: ConfigService<ConfigVariables>,
): RedisModuleOptions => ({
    closeClient: true,
    readyLog: true,
    config: {
        host: config.get('redisHost'),
        port: config.get('redisPort'),
    },
})
