import {
    CanActivate,
    ExecutionContext,
    ForbiddenException,
    Injectable,
} from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { AuthService } from '@auth/auth.service'

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private reflector: Reflector,
        private readonly authService: AuthService,
    ) {}
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const roles = this.reflector.getAllAndOverride<string[]>('roles', [
            context.getHandler(),
            context.getClass(),
        ])
        if (!roles) return true
        const req = context.switchToHttp().getRequest<Request>()
        const token = req.headers['authorization'].split(' ')[1]

        const { roles: userRoles } = this.authService.verifyToken(token)

        if (!userRoles.some(userRole => roles.includes(userRole))) {
            throw new ForbiddenException()
        }

        return true
    }
}
