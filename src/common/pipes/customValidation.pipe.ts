import {
    ArgumentMetadata,
    Injectable,
    PipeTransform,
    ValidationError,
} from '@nestjs/common'
import { plainToInstance } from 'class-transformer'
import { validate } from 'class-validator'
import { CustomValidationException } from '../exceptions/customValidation.exception'

@Injectable()
export class CustomValidationPipe implements PipeTransform {
    async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
        if (!value) {
            return value
        }
        if (!metadata.metatype || metadata.type !== 'body') {
            return value
        }

        const obj = plainToInstance(metadata.metatype, value)
        const errors = await validate(obj)

        if (errors.length) {
            const message = errors.map(err => {
                return { [err.property]: this.getFieldErrors(err) }
            })

            const result = {
                statusCode: 400,
                message: message.reduce((acc, cur) => ({ ...acc, ...cur }), {}),
                error: 'Validation error',
            }

            throw new CustomValidationException(result)
        }
        return obj
    }

    private getFieldErrors(error: ValidationError) {
        let propertyError
        let items
        if (error.constraints) {
            propertyError = Object.values(error.constraints).join(', ')
        }
        if (error.children && error.children.length > 0) {
            items = this.getChildrenErrors(error.children)
        }

        return { error: propertyError, items: items }
    }

    private getChildrenErrors(children: ValidationError[]): {
        [key: string]: any
    } {
        const items: { [key: string]: any } = {}

        for (let i = 0; i < children.length; i++) {
            if (children[i].children) {
                items[children[i].property] = {}
                for (const error of children[i].children!) {
                    items[children[i].property][error.property] =
                        this.getFieldErrors(error)
                }
            } else
                items[children[i].property] = this.getFieldErrors(children[i])
        }
        return items
    }
}
