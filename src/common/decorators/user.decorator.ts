import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { Request } from 'express'
import { AccessTokenPayloadBO } from '@auth/bos'

export const User = createParamDecorator(
    (data: string, ctx: ExecutionContext) => {
        const req = ctx
            .switchToHttp()
            .getRequest<Request & { user: AccessTokenPayloadBO }>()
        return data ? req?.user?.[data] : req.user
    },
)
