import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { Request } from 'express'

export const AccessToken = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
        const req = ctx
            .switchToHttp()
            .getRequest<Request & { accessToken: string }>()
        return req.accessToken
    },
)
