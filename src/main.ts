import { NestFactory } from '@nestjs/core'
import { NestExpressApplication } from '@nestjs/platform-express'
import { AppModule } from './app.module'
import { ConfigService } from '@nestjs/config'
import { ConfigVariables } from '@config'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { CustomValidationPipe } from '@common/pipes/customValidation.pipe'
import * as cookie from 'cookie-parser'

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule, {
        bufferLogs: true,
    })

    const convigService: ConfigService<ConfigVariables> = app.get(ConfigService)
    const PORT = convigService.get('port')

    const config = new DocumentBuilder()
        .setTitle('Pro-Financy')
        .setVersion('1.0')
        .addBearerAuth()
        .build()
    const document = SwaggerModule.createDocument(app, config)
    SwaggerModule.setup('swagger', app, document)

    app.use(cookie())
    app.useGlobalPipes(new CustomValidationPipe())
    app.enableShutdownHooks()
    app.disable('x-powered-by')
    app.enableCors({
        exposedHeaders: ['Content-Range', 'X-Content-Range'],
        credentials: true,
    })

    await app.listen(PORT)
}
bootstrap()
