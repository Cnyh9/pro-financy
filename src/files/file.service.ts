import {
    BadRequestException,
    Injectable,
    InternalServerErrorException,
} from '@nestjs/common'
import { v4 } from 'uuid'
import { MFile } from '@files/mfile'
import * as sharp from 'sharp'
import { join } from 'path'
import { access, mkdir, writeFile } from 'fs/promises'

@Injectable()
export class FileService {
    constructor() {}

    async filter(file: Express.Multer.File, folder: string): Promise<string> {
        const mimetype = file.mimetype
        const currentFileType = mimetype.split('/')[1]
        const newName = v4()
        const type = file.originalname.split('.')[1]
        if (!mimetype.includes('image'))
            throw new BadRequestException('This mimetype is not allowed')

        const buffer = await this.convertToWebP(file.buffer)
        const filteredFile = new MFile({
            buffer,
            originalname: `${newName}.webp`,
            mimetype,
        })

        return this.saveFile(filteredFile, folder)
    }

    private async saveFile(file: MFile, folder = 'default'): Promise<string> {
        const uploadFolder = join(__dirname, `../../static/${folder}`)

        try {
            await access(uploadFolder)
        } catch (e) {
            await mkdir(uploadFolder, { recursive: true })
        }

        try {
            await writeFile(join(uploadFolder, file.originalname), file.buffer)
        } catch (e) {
            throw new InternalServerErrorException('Ошибка при записи файла')
        }
        return file.originalname
    }

    private convertToWebP(file: Buffer): Promise<Buffer> {
        return sharp(file).webp().toBuffer()
    }
}
