export class MFile {
    buffer: Buffer
    mimetype: string
    originalname: string

    constructor(file: {
        originalname: string
        buffer: Buffer
        mimetype: string
    }) {
        this.buffer = file.buffer
        this.mimetype = file.mimetype
        this.originalname = file.originalname
    }
}
