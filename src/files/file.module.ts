import { Module } from '@nestjs/common'
import { ServeStaticModule } from '@nestjs/serve-static'
import { join } from 'path'
import { FileService } from '@files/file.service'

@Module({
    imports: [
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, `../../static`),
            serveRoot: 'static',
        }),
    ],
    providers: [FileService],
    exports: [FileService],
})
export class FileModule {}
