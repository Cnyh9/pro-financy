import { Factory, Seeder } from 'typeorm-seeding'
import { DataSource } from 'typeorm'
import { RoleEntity } from '../entities'
import { roles } from './initData/roles-init-data'

export default class CreateRolesInitData implements Seeder {
    async run(factory: Factory, connection: DataSource): Promise<void> {
        await connection.manager.transaction(async manager => {
            await manager
                .createQueryBuilder()
                .insert()
                .into(RoleEntity)
                .values(roles)
                .execute()
        })
    }
}
