import { RoleEntity } from '../../entities'

export const roles: Partial<RoleEntity[]> = [
    {
        role: 'user',
        description: 'Роль пользователя',
    },
    {
        role: 'admin',
        description: 'Роль администратора',
    },
]
