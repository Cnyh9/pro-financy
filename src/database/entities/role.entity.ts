import { Column, Entity, PrimaryColumn } from 'typeorm'
import { ApiProperty } from '@nestjs/swagger'

@Entity('roles')
export class RoleEntity {
    @ApiProperty()
    @PrimaryColumn('varchar')
    role: string

    @ApiProperty()
    @Column('varchar')
    description: string
}
