import { BeforeUpdate, Column, PrimaryGeneratedColumn } from 'typeorm'

export class Base {
    @PrimaryGeneratedColumn()
    id: number

    @Column('timestamptz', {
        default: () => 'CURRENT_TIMESTAMP',
        name: 'created_at',
    })
    createdAt: Date

    @Column('timestamptz', {
        default: () => 'CURRENT_TIMESTAMP',
        name: 'updated_at',
    })
    updatedAt: Date

    @BeforeUpdate()
    updateTimestamp() {
        this.updatedAt = new Date()
    }
}
