import { Column, Entity, JoinTable, ManyToMany } from 'typeorm'
import { Base } from './base'
import { ApiProperty } from '@nestjs/swagger'
import { RoleEntity } from './role.entity'

@Entity('users')
export class UserEntity extends Base {
    @ApiProperty()
    @Column('varchar', { unique: true })
    email: string

    @ApiProperty()
    @Column('varchar', { nullable: true })
    name: string

    @ApiProperty()
    @Column('varchar', { select: false })
    password: string

    @ApiProperty()
    @Column('varchar', { nullable: true })
    avatar: string

    @ManyToMany(() => RoleEntity)
    @JoinTable({
        name: 'user_roles',
        joinColumn: { name: 'user_id' },
        inverseJoinColumn: { name: 'role' },
    })
    roles: RoleEntity[]
}
