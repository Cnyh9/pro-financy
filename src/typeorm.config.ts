import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import { config } from 'dotenv'

config()

const {
    POSTGRES_PORT,
    POSTGRES_HOST,
    POSTGRES_USER,
    POSTGRES_DB,
    POSTGRES_PASSWORD,
} = process.env

const ormconfig: TypeOrmModuleOptions & { seeds: string[] } = {
    type: 'postgres',
    host: POSTGRES_HOST,
    port: +POSTGRES_PORT,
    database: POSTGRES_DB,
    username: POSTGRES_USER,
    password: POSTGRES_PASSWORD,
    synchronize: false,
    entities: [`${__dirname}/database/entities/*.entity.{ts,js}`],
    migrations: [`${__dirname}/database/migrations/*.{ts,js}`],
    seeds: [`${__dirname}/database/seeds/*.{ts,js}`],
}

export default ormconfig
